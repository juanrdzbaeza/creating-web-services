package packt.jee.eclipse.rest.ws.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class CourseManagementRESTClient {

	public static void main(String[] args) {
		//testGetCoursesJSON();
		//testAddCourseJSON();
		testAddCourseForm();
	}
	
	public static void testGetCoursesJSON() {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8080/CourseManagementREST/services/course");
		webTarget = webTarget.path("get").path("10");
		client.target("http://localhost:8080/CourseManagementREST/services/course/get/10");
		Response response = webTarget.request(MediaType.APPLICATION_JSON).get();
		if (response.getStatus() != 200) {
			System.out.println("Error invoking REST Web Service - " + response.getStatusInfo().getReasonPhrase());
			return;
		}
		System.out.println(response.readEntity(String.class));
	}
	
	public static void testAddCourseJSON() {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8080/CourseManagementREST/services/course/add");
		String courseJSON = "{\"course_name\":\"Course-4\", \"credits\":5}";
		Response response = webTarget.request().post(Entity.entity(courseJSON, MediaType.APPLICATION_JSON_TYPE));
		if (response.getStatus() != 200) {
			System.out.println("Error invoking REST Web Service - " +  response.getStatusInfo().getReasonPhrase() + ", Error Code : " + response.getStatus());
			System.out.println(response.readEntity(String.class));
			return;
		}
		System.out.println(response.readEntity(String.class));
	}

	public static void testAddCourseForm() {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8080/CourseManagementREST/services/course/add");
		Form form = new Form();
		form.param("name", "Course-5");
		form.param("credits", "5");
		Response response = webTarget.request().post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED));
		if (response.getStatus() != 200) {
			System.out.println("Error invoking REST Web Service - " +  response.getStatusInfo().getReasonPhrase() + ", Error Code : " + response.getStatus());
			System.out.println(response.readEntity(String.class));
			return;
		}
		System.out.println(response.readEntity(String.class));
	}
}
